# Linear temporal logic Constrains of an autonomous mobile robots



## Getting started

I used for this project nuSMV2.6.0 you need to download it and create an environment anaconda in order to install all the dependencies and the nuSMV from [here](https://nusmv.fbk.eu/bin/bin_download2-v2.cgi).

## files

## 1. Original.smv: 

Only to show the nuSMV structure just use the work.smv for evaluation Original file used for the model checking.

![Example Image](img/img1.png)
## 2. generate.py: 

Use it to generate with the preferred variable to send to the smv using the script, the variable represent the range of time
for each station to use for an envoirement with 5 stations and the initial configuration having different constrains 
between the stations with specific constrains. You can also have an initial configuration that is not allowed e.g choosing
the loaded material so if you want to check the default configuration (all Zero values) insert 1 instead 0 when asked.
The parameters of this configuration are choosen between the Max value for timer values that models how a robot can
cooperate with stations. To do not waste time respecting the fairness conditions, for these reason is not allowed for the user
to choose a configuration doesn't allowed.

![Example Image](img/img2.png)
## 3.work.smv:
generated file to test using:
```
read_model -i <file-name>
flatten_hierarchy
encode_variables
build_model
pick_states -i
simulate -i
```
![Example Image](img/img3.png)

## 4. Read the states:

position = position of the robot between the stations.

epsilon = charged material from the robot.

previous_state = previous station visited.

gamma1 = timer 1.

gamma2 = timer 2.

gamma3 = timer 3.

gamma4 = timer 4.

gamma5 = timer 5.

gamma6 = timer 6.

counter = number of product created.
