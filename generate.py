f = open("work.smv", "w")

default_conf = float(input("DO YOU WANT TO USE DEFAULT CONFIGURTION? (1=YES,0=NO): \n"))#10

init_epsilon = 0;
init_pos = 0;

if(default_conf==1):
    maxG1 = 10;
    maxG2 = 30;
    maxG3 = 25;
    maxG4 = 40;
    maxG5 = 45;
else:
    maxG1 = float(input("TIMER: Please enter a max value for timer 1 : \n"))#10
    maxG2 = float(input("TIMER:  Please enter a max value for timer 2: \n"))#30
    maxG3 = float(input("TIMER: Please enter a max value for timer 3: \n"))#25
    maxG4 = float(input("TIMER: Please enter a max value for timer 4: \n"))#40
    maxG5 = float(input("TIMER: Please enter a max value for timer 5: \n"))#45

f.write(" MODULE main \n");

f.write("VAR \n");

f.write("position: {0,1,2,3,4}; \n");

f.write("epsilon: 0 .. 4; \n");

f.write("previous_state: {0,1,2,3,4}; \n");

f.write("gamma1: 0 .. 100; \n");
f.write("gamma2: 0 .. 100; \n");
f.write("gamma3: 0 .. 100; \n");
f.write("gamma4: 0 .. 100; \n");
f.write("gamma5: 0 .. 100; \n");
f.write("gamma6: {0,1}; \n");
f.write("counter: 0 .. 1000; \n");

f.write("ASSIGN \n");

f.write("init(position) := "+str(int(init_pos))+"; \n");
f.write("init(gamma1) := 0; \n");
f.write("init(gamma2) := 0; \n");
f.write("init(gamma3) := 0; \n");
f.write("init(gamma4) := 0; \n");
f.write("init(gamma5) := 0; \n");
f.write("init(gamma6) := 0; \n");
f.write("init(previous_state) := 0; \n");
f.write("init(epsilon) := "+str(int(init_epsilon))+"; \n");
f.write("init(counter) := 0; \n");

f.write("next(position) := case \n");
f.write("(position = 0) & (epsilon = 1) & (gamma1 = 0): 1;  \n");

f.write("(position = 1) & (epsilon = 0) & (gamma1 > 0) & (gamma1 > "+str(int(maxG1/4))+") & (gamma1 < "+str(int(maxG1/2))+") & (gamma2 > 0): 2; \n");
f.write("(position = 1) & (epsilon = 0) & (gamma1 > 0) & (gamma1 > "+str(int(maxG1/2))+") & (gamma1 < "+str(int(maxG1))+") & (gamma3 > 0): 3; \n");
f.write("(position = 1) & (epsilon = 2) & !(gamma3 = 0) & (gamma2 = 0): 2; \n");
f.write("(position = 1) & (epsilon = 2) & !(gamma2 = 0) & (gamma3 = 0): 3; \n");
f.write("(position = 1) & (epsilon = 2) & (gamma2 = 0) & (gamma3 = 0): 2; \n");
               
f.write("(position = 2) & (epsilon = 0) & (gamma2 > 0) & (gamma2 < "+str(int(maxG2/4))+") & (gamma1 = 0): 0; \n");
f.write("(position = 2) & (epsilon = 0) & (gamma2 > "+str(int(maxG2/4))+") & (gamma2 < "+str(int(maxG2/2))+") & (gamma1 > 0): 1; \n");
f.write("(position = 2) & (epsilon = 0) & (gamma2 > "+str(int(maxG2/2))+") & (gamma2 < "+str(int(maxG2))+") & (gamma3 > 0): 3; \n");
f.write("(position = 2) & (epsilon = 3) & (gamma4 = 0): 4; \n");
               
f.write("(position = 3) & (epsilon = 0) & (gamma3 > 0) & (gamma3 < "+str(int(maxG3/4))+") & (gamma1 = 0): 0; \n");
f.write("(position = 3) & (epsilon = 0) & (gamma3 > "+str(int(maxG3/4))+") & (gamma3 < "+str(int(maxG3/2))+") & (gamma1 = "+str(int(maxG1))+"): 1; \n");
f.write("(position = 3) & (epsilon = 0) & (gamma3 > "+str(int(maxG3/2))+") & (gamma3 < "+str(int(maxG3))+") & (gamma2 = "+str(int(maxG2))+"): 2; \n");
f.write("(position = 3) & (epsilon = 4) & (gamma5 = 0): 4; \n");
               
f.write("(position = 4) & (epsilon = 0) & (gamma4 > "+str(int(maxG4/2))+") & (gamma4 <= "+str(int(maxG4))+") & (gamma1 = 0): 0; \n");
f.write("(position = 4) & (epsilon = 0) & (gamma4 > "+str(int(maxG4/4))+") & (gamma4 < "+str(int(maxG4/2))+") & (gamma1 = "+str(int(maxG1))+"): 1; \n");
f.write("(position = 4) & (epsilon = 0) & (gamma4 > "+str(int(maxG4/6))+") & (gamma4 < "+str(int(maxG4/4))+") & (gamma3 = "+str(int(maxG3))+"): 3; \n");
f.write("(position = 4) & (epsilon = 0) & (gamma4 > 0) & (gamma4 < "+str(int(maxG4/6))+") & (gamma5 > 0): 0; \n");
               
f.write("(position = 4) & (epsilon = 0) & (gamma5 > "+str(int(maxG5/2))+") & (gamma5 <= "+str(int(maxG5))+") & (gamma1 = 0): 0; \n");
f.write("(position = 4) & (epsilon = 0) & (gamma5 > "+str(int(maxG5/4))+") & (gamma5 < "+str(int(maxG5/2))+") & (gamma1 = "+str(int(maxG1))+"): 1; \n");
f.write("(position = 4) & (epsilon = 0) & (gamma5 > "+str(int(maxG5/6))+") & (gamma5 < "+str(int(maxG5/4))+") & (gamma2 = "+str(int(maxG2))+"): 2; \n");
f.write("(position = 4) & (epsilon = 0) & (gamma5 > 0) & (gamma5 < "+str(int(maxG5/6))+") & (gamma4 > 0): 0; \n");
               
f.write("(position = 4) & (gamma5 = "+str(int(maxG5))+") & (position = 4) & (gamma4 = "+str(int(maxG4))+"): 0; \n");
f.write("(position = 4) & (gamma5 = "+str(int(maxG5))+") & (position = 4) & (gamma4 = "+str(int(maxG4))+"): 0; \n");


#for e in range(1,4):
#    f.write("(position = "+str(e)+") & (epsilon = 0) & (gamma"+str(int(e+1))+" = 0): "+str(int(e-1))+"; \n");
               
f.write("TRUE: position; \n");
f.write("esac; \n");

f.write("next(gamma1) := case \n");

f.write("(((position = 2) & (previous_state = 1)) | ((position = 3) & (previous_state = 1)) | ((position = 0) & (previous_state = 1))) & (gamma1 > 0) & (gamma1 < "+str(int(maxG1))+"): gamma1 + 5; \n");
f.write("(((position = 1) & (previous_state = 2)) | ((position = 3) & (previous_state = 2)) | ((position = 0) & (previous_state = 2))) & (gamma1 > 0) & (gamma1 < "+str(int(maxG1))+"): gamma1 + 5; \n");
f.write("(((position = 2) & (previous_state = 3)) | ((position = 1) & (previous_state = 3)) | ((position = 0) & (previous_state = 3)))  & (gamma1 > 0) & (gamma1 < "+str(int(maxG1))+"): gamma1 + 5; \n");
f.write("(((position = 3) & (previous_state = 4)) | ((position = 1) & (previous_state = 4)) | ((position = 0) & (previous_state = 4))) & (gamma1 > 0) & (gamma1 < "+str(int(maxG1))+"): gamma1 + 2; \n");
f.write("(((position = 2) & (previous_state = 4)) | ((position = 1) & (previous_state = 4)) | ((position = 0) & (previous_state = 4))) & (gamma1 > 0) & (gamma1 < "+str(int(maxG1))+"): gamma1 + 5; \n");

f.write("(position = 1) & (gamma1 = 0)  & (epsilon = 1) : gamma1 + 1; \n");
f.write("(gamma1 > 0) & (gamma1 < 10) : gamma1 + 1; \n");
f.write("(gamma1 >= 10) & (position = 1) & (epsilon = 0) & (gamma2 = 0):  0; \n");
f.write("(gamma1 >= 10) & (position = 1) & (epsilon = 0) & (gamma3 = 0):  0; \n");
        
f.write("TRUE: gamma1; \n");
f.write("esac; \n");

f.write("next(gamma2) := case \n");
        
f.write("(((position = 2) & (previous_state = 1)) | ((position = 3) & (previous_state = 1)) | ((position = 0) & (previous_state = 1))) & (gamma2 > 0) & (gamma2 < "+str(int(maxG2))+"): gamma2 + 5; \n");
f.write("(((position = 1) & (previous_state = 2)) | ((position = 3) & (previous_state = 2)) | ((position = 0) & (previous_state = 2))) & (gamma2 > 0) & (gamma2 < "+str(int(maxG2))+"): gamma2 + 5; \n");
f.write("(((position = 2) & (previous_state = 3)) | ((position = 1) & (previous_state = 3)) | ((position = 0) & (previous_state = 3)))  & (gamma2 > 0) & (gamma2 < "+str(int(maxG2))+"): gamma2 + 5; \n");
f.write("(((position = 3) & (previous_state = 4)) | ((position = 1) & (previous_state = 4)) | ((position = 0) & (previous_state = 4))) & (gamma2 > 0) & (gamma2 < "+str(int(maxG2))+"): gamma2 + 2; \n");
f.write("(((position = 2) & (previous_state = 4)) | ((position = 1) & (previous_state = 4)) | ((position = 0) & (previous_state = 4))) & (gamma2 > 0) & (gamma2 < "+str(int(maxG2))+"): gamma2 + 5; \n");




f.write("(gamma2 = 0) & (position = 2) & (epsilon = 2) : gamma2 + 1; \n");
f.write("(gamma2 > 0) & (gamma2 < 30) : gamma2 + 1; \n");
f.write("(gamma2 >= 30) & (position = 2) & (epsilon = 0) & (gamma4 = 0): 0; \n");
        
f.write("TRUE: gamma2; \n");
f.write("esac; \n");

f.write("next(gamma3) := case \n");
        
f.write("(((position = 2) & (previous_state = 3)) | ((position = 1) & (previous_state = 3)) | ((position = 0) & (previous_state = 3)))  & (gamma3 > 0) & (gamma3 < "+str(int(maxG3))+"): gamma3 + 5; \n");
f.write("(((position = 2) & (previous_state = 1)) | ((position = 3) & (previous_state = 1)) | ((position = 0) & (previous_state = 1))) & (gamma3 > 0) & (gamma3 < "+str(int(maxG3))+"): gamma3 + 5; \n");
f.write("(((position = 1) & (previous_state = 2)) | ((position = 3) & (previous_state = 2)) | ((position = 0) & (previous_state = 2))) & (gamma3 > 0) & (gamma3 < "+str(int(maxG3))+"): gamma3 + 5; \n");
f.write("(((position = 3) & (previous_state = 4)) | ((position = 1) & (previous_state = 4)) | ((position = 0) & (previous_state = 4))) & (gamma3 > 0) & (gamma3 < "+str(int(maxG3))+"): gamma3 + 2; \n");
f.write("(((position = 2) & (previous_state = 4)) | ((position = 1) & (previous_state = 4)) | ((position = 0) & (previous_state = 4))) & (gamma3 > 0) & (gamma3 < "+str(int(maxG3))+"): gamma3 + 5; \n");

f.write("(gamma3 = 0) & (position = 3) & (epsilon = 2) :  gamma3 + 1; \n");
f.write("(gamma3 > 0) & (gamma3 < 25)  : gamma3 + 1; \n");
f.write("(gamma3 >= 25) & (position = 3)  & (epsilon = 0) & (gamma5 = 0):  0; \n");
f.write("TRUE: gamma3; \n");
f.write("esac; \n");

f.write("next(gamma4) := case \n");

f.write("(((position = 3) & (previous_state = 4)) | ((position = 1) & (previous_state = 4)) | ((position = 0) & (previous_state = 4))) & (gamma4 > 0) & (gamma4 < "+str(int(maxG4))+"): gamma4 + 2; \n");
f.write("(((position = 2) & (previous_state = 1)) | ((position = 3) & (previous_state = 1)) | ((position = 0) & (previous_state = 1))) & (gamma4 > 0) & (gamma4 < "+str(int(maxG4))+"): gamma4 + 5; \n");
f.write("(((position = 1) & (previous_state = 2)) | ((position = 3) & (previous_state = 2)) | ((position = 0) & (previous_state = 2))) & (gamma4 > 0) & (gamma4 < "+str(int(maxG4))+"): gamma4 + 5; \n");
f.write("(((position = 2) & (previous_state = 3)) | ((position = 1) & (previous_state = 3)) | ((position = 0) & (previous_state = 3)))  & (gamma4 > 0) & (gamma4 < "+str(int(maxG4))+"): gamma4 + 5; \n");
f.write("(((position = 2) & (previous_state = 4)) | ((position = 1) & (previous_state = 4)) | ((position = 0) & (previous_state = 4))) & (gamma4 > 0) & (gamma4 < "+str(int(maxG4))+"): gamma4 + 5; \n");


f.write("(gamma4 = 0) & (position = 4) & (epsilon = 3) :  gamma4 + 1; \n");
f.write("(gamma4 > 0) & (gamma4 < 40): gamma4 + 1; \n");
f.write("(gamma6 = 1) : 0; \n");

f.write("TRUE: gamma4; \n");
f.write("esac; \n");

f.write("next(gamma5) := case \n");

f.write("(((position = 3) & (previous_state = 4)) | ((position = 1) & (previous_state = 4)) | ((position = 0) & (previous_state = 4))) & (gamma5 > 0) & (gamma5 < "+str(int(maxG5))+"): gamma5 + 2; \n");
f.write("(((position = 2) & (previous_state = 1)) | ((position = 3) & (previous_state = 1)) | ((position = 0) & (previous_state = 1))) & (gamma5 > 0) & (gamma5 < "+str(int(maxG5))+"): gamma5 + 5; \n");
f.write("(((position = 1) & (previous_state = 2)) | ((position = 3) & (previous_state = 2)) | ((position = 0) & (previous_state = 2))) & (gamma5 > 0) & (gamma5 < "+str(int(maxG5))+"): gamma5 + 5; \n");
f.write("(((position = 2) & (previous_state = 3)) | ((position = 1) & (previous_state = 3)) | ((position = 0) & (previous_state = 3)))  & (gamma5 > 0) & (gamma5 < "+str(int(maxG5))+"): gamma5 + 5; \n");
f.write("(((position = 2) & (previous_state = 4)) | ((position = 1) & (previous_state = 4)) | ((position = 0) & (previous_state = 4))) & (gamma5 > 0) & (gamma5 < "+str(int(maxG5))+"): gamma5 + 5; \n");

        
f.write("(gamma5 = 0) & (position = 4) & (epsilon = 4) :  gamma5 + 1; \n");
f.write("(gamma5 > 0) & (gamma5 < "+str(int(maxG5))+") : gamma5 + 1; \n");
f.write("(gamma6 = 1) :  0; \n");
f.write("TRUE: gamma5; \n");
f.write("esac; \n");

f.write("next(gamma6) := case \n");
f.write("(gamma5 = 0) & (gamma4 = 0)  : 0; \n");
f.write("(gamma5 >= "+str(int(maxG5))+") & (gamma4 >= "+str(int(maxG4))+")  : 1; \n");
f.write("TRUE: gamma6; \n");
f.write("esac; \n");

f.write("next(counter) := case \n");
f.write("(gamma6 = 1) & (counter < 100) : counter+1; \n");
f.write("TRUE: counter; \n");
f.write("esac; \n");



f.write("next(epsilon) := case \n");
f.write("(position = 0) & (epsilon = 0) & (gamma1 = 0): 1; \n");
f.write("(position = 1) & (epsilon = 1): 0; \n");
f.write("(position = 2) & (epsilon = 2): 0; \n");
f.write("(position = 3) & (epsilon = 2): 0; \n");
f.write("(position = 4) & (epsilon = 3): 0; \n");
f.write("(position = 4) & (epsilon = 4): 0; \n");

f.write("(position = 1) & (epsilon = 0) & (gamma1 >=  "+str(int(maxG1))+") & (gamma3 = 0):  2; \n");
f.write("(position = 1) & (epsilon = 0) & (gamma1 >=  "+str(int(maxG1))+") & (gamma2 = 0):  2; \n");
f.write("(position = 1) & (epsilon = 0) & (gamma1 >= "+str(int(maxG1))+") & (gamma2 = 0)  & (gamma3 = 0):  2; \n");
f.write("(position = 2) & (epsilon = 0) & (gamma2 >=  "+str(int(maxG2))+") & (gamma4 = 0):  3; \n");
f.write("(position = 3) & (epsilon = 0) & (gamma3 >=  "+str(int(maxG3))+") & (gamma5 = 0):  4; \n");
f.write("TRUE: epsilon; \n");
f.write("esac; \n");

f.write("next(previous_state) := case \n");
        
f.write("(position = 0) : 0; \n");
f.write("(position = 1) : 1; \n");
f.write("(position = 2) : 2; \n");
f.write("(position = 3) : 3; \n");
f.write("(position = 4) : 4; \n");

f.write("TRUE: previous_state; \n");
f.write("esac; \n");

f.write("LTLSPEC \n");
f.write("!(epsilon = 0) & (gamma2 > 0) & (gamma2 < "+str(int(maxG2))+") \n");
f.write("LTLSPEC \n");
f.write("!(epsilon = 0) & (gamma1 > 0) & (gamma1 < "+str(int(maxG1))+") \n");
f.write("LTLSPEC \n");
f.write("!(epsilon = 0) & (gamma3 > 0) & (gamma3 < "+str(int(maxG3))+") \n");
f.write("LTLSPEC \n");
f.write("!(epsilon = 0) & (gamma4 > 0) & (gamma5 < "+str(int(maxG4))+") \n");
f.write("LTLSPEC \n");
f.write("!(epsilon = 0) & (gamma5 > 0) & (gamma4 < "+str(int(maxG5))+") \n");
#f.write("LTLSPEC \n");
#f.write("(epsilon = 1) & (!(position = p0) | !(position = p1)) \n");
#f.write("LTLSPEC \n");
#f.write("(epsilon = 2) & (!(position = p1) | !(position = p2) | !(position = p3)) \n");
#f.write("LTLSPEC \n");
#f.write("(epsilon = 3) & (!(position = p2) | !(position = p4)) \n");
#f.write("LTLSPEC \n");
#f.write("(epsilon = 4) & (!(position = p3) | !(position = p5)) \n");

        
f.close();
